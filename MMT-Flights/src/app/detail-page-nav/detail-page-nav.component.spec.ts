import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetailPageNavComponent } from './detail-page-nav.component';

describe('DetailPageNavComponent', () => {
  let component: DetailPageNavComponent;
  let fixture: ComponentFixture<DetailPageNavComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetailPageNavComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetailPageNavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
