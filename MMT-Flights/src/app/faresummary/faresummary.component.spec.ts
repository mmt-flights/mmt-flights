import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FaresummaryComponent } from './faresummary.component';

describe('FaresummaryComponent', () => {
  let component: FaresummaryComponent;
  let fixture: ComponentFixture<FaresummaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FaresummaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FaresummaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
