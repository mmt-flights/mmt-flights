import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FlightseatingComponent } from './flightseating.component';

describe('FlightseatingComponent', () => {
  let component: FlightseatingComponent;
  let fixture: ComponentFixture<FlightseatingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FlightseatingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FlightseatingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
